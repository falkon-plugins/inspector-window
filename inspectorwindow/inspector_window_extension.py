# ============================================================
# Inspector Window extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtCore, QtWidgets
from PySide2.QtGui import QIcon


class InspectorWindowExtension(Falkon.PluginInterface, QtCore.QObject):
    inspectors = {}

    def init(self, state, path):
        self.settings = QtCore.QSettings(path + "/extensions.ini", QtCore.QSettings.IniFormat)

    def unload(self):
        pass

    def testPlugin(self):
        return Falkon.WebInspector.isEnabled()

    def should_be_ontop(self):
        set_val = self.settings.value("InspectorWindow/keep-on-top", "0")
        return not int(set_val) == 0

    def on_focus_changed_cb(self, focus):
        inspector = None
        try:
            inspector = self.inspectors[self.view]
        except KeyError:
            return
        onfocus = QtWidgets.QApplication.focusWidget()
        if onfocus:
            if onfocus.parentWidget() == inspector and \
                                         self.view.webTab().haveInspector():
                self.view.webTab().toggleWebInspector()
                inspector.setView(self.view)

    def on_inspector_closed_cb(self):
        self.inspectors.pop(self.view, None)

    def on_show_inspector_cb(self):
        if self.view in self.inspectors:
            inspector = self.inspectors[self.view]
            inspector.raise_()
        else:
            flags = QtCore.Qt.Window
            if self.should_be_ontop():
                flags |= QtCore.Qt.WindowStaysOnTopHint
            inspector = Falkon.WebInspector(parent=self.view)
            inspector.setWindowFlags(flags)
            inspector.setWindowTitle("Inspector for \"%s\" page" % self.view.title())
            inspector.setWindowIcon(QIcon.fromTheme("preferences-system-windows"))
            inspector.setView(self.view)
            inspector.destroyed.connect(self.on_inspector_closed_cb)
            self.view.focusChanged.connect(self.on_focus_changed_cb)
            self.inspectors[self.view] = inspector
            inspector.show()

    def populateWebViewMenu(self, menu, view, r):
        self.view = view
        menu.addAction("Inspect element in new window", self.on_show_inspector_cb)

    def showSettings(self, widget):
        stngs_dlg = QtWidgets.QDialog(parent=widget)
        stngs_dlg.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        stngs_dlg.setWindowTitle("Inspector Window Extension Settings")

        chck_box = QtWidgets.QCheckBox("Keep Inspector window on top")
        chck_box.setToolTip("If Inspector Window is already opened,"
                            " it should be closed and opened again\n"
                            " in order this setting to take effect!")
        chck_box.stateChanged.connect(lambda v: self.settings.setValue(
                                      "InspectorWindow/keep-on-top", v))
        chck_box.setChecked(self.should_be_ontop())

        close_btn = QtWidgets.QPushButton("Close")
        close_btn.clicked.connect(stngs_dlg.close)

        lout = QtWidgets.QVBoxLayout(stngs_dlg)
        lout.addWidget(chck_box)
        lout.addWidget(close_btn)
        lout.setAlignment(close_btn, QtCore.Qt.AlignRight)

        stngs_dlg.show()


Falkon.registerPlugin(InspectorWindowExtension())
